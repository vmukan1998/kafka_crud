import data.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class Main {

    public static void main(String[] args) {

        Thread consumerThread = new Thread(new Consumer());
        Thread producerThread = new Thread(new Producer());

        //consumerThread.start();
        producerThread.start();


    }
}
