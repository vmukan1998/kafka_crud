package DbManipulations;

import data.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class MySqlDataManipulator {

    private Properties properties;
    private String path;

    public MySqlDataManipulator(String path, Properties properties) {
        this.properties = properties;
        this.path = path;
    }

    public List<User> read() {

        List<User> data = new ArrayList<>();

        try (Connection connection = DriverManager.getConnection(path, properties);
            Statement statement = connection.createStatement()
        ) {
            String selectQuery = "SELECT id, first_name, last_name, email, gender, ip_address FROM users";
            ResultSet resultSet = statement.executeQuery(selectQuery);

            while (resultSet.next()) {
                data.add(new User(
                        resultSet.getInt("id"),
                        resultSet.getString("first_name"),
                        resultSet.getString("last_name"),
                        resultSet.getString("email"),
                        resultSet.getString("gender"),
                        resultSet.getString("ip_address")
                ));
            }
            ;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return data;
    }


    public void write(List<User> users) {

        String insertQuery = "INSERT INTO users1(id, first_name," +
                "last_name, email, gender, ip_address) VALUES (?,?,?,?,?,?)";
        try (Connection connection = DriverManager.getConnection(path, properties);
             Statement statement = connection.createStatement();
             PreparedStatement preparedSt = connection.prepareStatement(insertQuery)
        ) {
//            statement.executeUpdate(
//                    "CREATE TABLE IF NOT EXISTS users1 ( id INT, first_name VARCHAR(50), " +
//                            "last_name VARCHAR(50), email VARCHAR(50)," +
//                            "gender VARCHAR(50),ip_address VARCHAR(20));");
//            statement.executeUpdate("TRUNCATE TABLE users1");

            for (User user : users) {
                preparedSt.setInt(1, user.getId());
                preparedSt.setString(2, user.getFirstName());
                preparedSt.setString(3, user.getLastName());
                preparedSt.setString(4, user.getEmail());
                preparedSt.setString(5, user.getGender());
                preparedSt.setString(6, user.getIpAdress());

                preparedSt.execute();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Properties getProperties() {
        return properties;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
