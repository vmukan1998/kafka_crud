import DbManipulations.MySqlDataManipulator;

import data.User;
import org.apache.avro.generic.GenericRecord;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

public class Consumer implements Runnable{

    @Override
    public void run() {
        Properties dbProp = new Properties();
        dbProp.setProperty("user", "root");
        dbProp.setProperty("password", "root");
        dbProp.setProperty("useSSL", "false");
       // dbProp.setProperty("autoReconnect", "true");

        String dbPath = "jdbc:mysql://localhost/eleks";
        MySqlDataManipulator manipulator = new MySqlDataManipulator(dbPath, dbProp);

        String topicName = "task1";
        Properties kafkaProp = new Properties();

        kafkaProp.put("bootstrap.servers", "localhost:9092");
        kafkaProp.put("group.id", "new-group");
        kafkaProp.put("key.deserializer", "io.confluent.kafka.serializers.KafkaAvroDeserializer");
        kafkaProp.put("value.deserializer", "io.confluent.kafka.serializers.KafkaAvroDeserializer");
        kafkaProp.put("schema.registry.url","http://localhost:8081");
        //properties.put(ConsumerConfig.)

        KafkaConsumer consumer = new KafkaConsumer<GenericRecord, GenericRecord>(kafkaProp);
        consumer.subscribe(Collections.singletonList(topicName));

        List<User> userList = new ArrayList<>();

        while (!Thread.currentThread().isInterrupted()) {
            ConsumerRecords<GenericRecord, GenericRecord> records = consumer.poll(1000);

            for (ConsumerRecord<GenericRecord, GenericRecord> record: records) {
                //System.out.println(record.value().get("ip_address"));
                userList.add(new User(
                        (int)record.value().get("id"),
                        record.value().get("first_name").toString(),
                        record.value().get("last_name").toString(),
                        record.value().get("email").toString(),
                        record.value().get("gender").toString(),
                        record.value().get("ip_address").toString()
                ));
            }
            consumer.commitSync();
            manipulator.write(userList);
        }

    }
}
