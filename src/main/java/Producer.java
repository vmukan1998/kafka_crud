import DbManipulations.MySqlDataManipulator;
import data.User;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;


public class Producer implements Runnable{

    @Override
    public void run() {

        Properties dbProp = new Properties();
        dbProp.setProperty("user", "root");
        dbProp.setProperty("password", "root");
        dbProp.setProperty("useSSL", "false");

        String dbPath = "jdbc:mysql://localhost/eleks";
        MySqlDataManipulator manipulator = new MySqlDataManipulator(dbPath, dbProp);

        List<User> data = manipulator.read();

        String topic = "task1";
        Properties kafkaProp = new Properties();

        kafkaProp.put("bootstrap.servers", "localhost:9092");
        kafkaProp.put("key.serializer", "io.confluent.kafka.serializers.KafkaAvroSerializer");
        kafkaProp.put("value.serializer", "io.confluent.kafka.serializers.KafkaAvroSerializer");
        kafkaProp.put("schema.registry.url", "http://localhost:8081");
        kafkaProp.put("acks", "all");

        KafkaProducer<GenericRecord, GenericRecord> producer = new KafkaProducer<>(kafkaProp);

        Schema schema = null;

        File avroFile = new File("src/main/avro/UserSchema.avsc");

        try {
            schema = new Schema.Parser().parse(avroFile);
        } catch(IOException e) {
            e.printStackTrace();
        }
//                SchemaBuilder.record("user")
//                .fields()
//                .name("id").type().intType().noDefault()
//                .name("first_name").type().stringType().noDefault()
//                .name("last_name").type().stringType().noDefault()
//                .name("email").type().stringType().noDefault()
//                .name("gender").type().stringType().noDefault()
//                .name("ip_address").type().stringType().noDefault()
//                .endRecord();

        ProducerRecord<GenericRecord, GenericRecord> producerRecord = null;
        GenericRecord genericRecord = null;

        for (User user : data) {
            genericRecord = new GenericData.Record(schema);
            genericRecord.put("id", user.getId());
            genericRecord.put("first_name", user.getFirstName());
            genericRecord.put("last_name", user.getLastName());
            genericRecord.put("email", user.getEmail());
            genericRecord.put("gender", user.getGender());
            genericRecord.put("ip_address", user.getIpAdress());

            producerRecord = new ProducerRecord<>(topic, null, genericRecord);
            producer.send(producerRecord);
        }


    }
}
